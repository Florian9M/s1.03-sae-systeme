# S1.03 SAE Système

Dans ce projet je vous présente mon environnement de travail me permettant de réaliser les travaux de mes différents cours (python, SQL, java, bash, web,..)

# Création de la Machine Virtuelle

Pour commencer j'ai choisis de créer une machine virtuelle avec VMWARE.

-   La version utilisé est "VMware Workstation 16 Player"

    -   J'ai du configuré ma machine virtuelle en faisant des choix sur le type de distribution, son environnement de bureau, la RAM, l'espace et le nombre de processeurs à aloué.

        -   **DISTRIBUTION :** Ubuntu version 20.04.1
        -   **ENVIRONNEMENT DE BUREAU :** GNOME
        -   **RAM :** 16Go
        -   **PROCESSEUR :** 4 processeurs logique
        -   **ESPACE :** 100Go

        | Distribution           | Envrionnement de bureau | RAM  | Processeur            | Espace |
        | ---------------------- | ----------------------- | ---- | --------------------- | ------ |
        | Ubuntu version 20.04.1 | GNOME                   | 16Go | 4 processeurs logique | 100Go  |

# Configuration de la Machine Virtuelle

J'ai décider de moderniser l'OS et certains logicel en ajoutant des thèmes et des fonctionalités pour les rendre plus puissant et ergonomique. Le but est de devenir plus productif.

Pour l'ergonomie et le design : 

-   Ajout du Draculatheme sur :
    -   le terminal
    -   le systeme de fichier 
    -   gedit

### Environnement de bureau

![Environnement bureau](https://image.noelshack.com/fichiers/2021/40/1/1633364369-screenshot-from-2021-10-04-18-09-49.png)

### Gedit

![Gedit](https://image.noelshack.com/fichiers/2021/40/1/1633364375-screenshot-from-2021-10-04-18-17-36.png)

### Visual studio Code

![VsCode](https://image.noelshack.com/fichiers/2021/40/1/1633364372-screenshot-from-2021-10-04-18-10-42.png)

### Systeme de fichier

![Systeme de fichier](https://image.noelshack.com/fichiers/2021/40/1/1633364378-screenshot-from-2021-10-04-18-18-48.png)

-   Installation d'extensions sur Visual Studio Code
    -   Material Theme Icons
    -   Material Theme
    -   LiveServer (utile pour avoir un rafraichissement en direct lors d'un changement sur son site)
    -   Docstring (utile pour créer des doctrings dans les fonctions python)

***


-   Changement police d'écriture dans le terminal et gedit
-   Changement de fond d'écran sur le bureau

Pour le rendre plus puissant : 

-   J'ai configurer plusieurs raccourcis clavier pour me permettre d'être plus rapide lors de l'ouverture d'une application. 

    | Raccourci    | Description                 |
    | ------------ | --------------------------- |
    | Super+R      | ouverture du navigateur web |
    | Ctrl+shift+v | ouverture de VScode         |

# Installation langages et application

**BASH** Le bash est nativement intégré dans Linux

**PYTHON** Python était installé aussi par default

**JAVA** Java n'était pas installé j'ai du l'installer avec la commande suivante : `sudo apt install openjdk-11-jdk`

**SNAPCRAFT** J'ai découvert un app store qui s'appelle SnapCraft. Il est très utile pour installer des paquets. SnapCraft rend les installations plus simples.\
J'ai installé snap grâce a la commande : `sudo apt install snapd`

**DOCKER** Docker n'était pas installé par default j'ai donc executé la commande : `sudo snap install docker`. Après l'installation j'ai juste eu besoin de me connecter avec la commande `docker login` et de renseigner mon id et mon mot de passe.



# Installation de Ubuntu 20.04 LTS sans environnement de bureau.

J'ai aussi pour tester, installé Ubuntu 20.04 sur le Microsoft Store.\
Cette alternative est très bien elle peut être utile pour utiliser git, bash, ssh, apt, nano et d'autres.\
L'inconvénient est qu'il n'y a pas d'environnement de bureau.

![Ubuntu](https://image.noelshack.com/fichiers/2021/41/2/1634041044-download.png)

# Le rapport

Le rapport est disponible [ici](https://docs.google.com/document/d/1xfuQ4Bgi_gykfTCS-svxOpvXm6CTtVWsbT1kiauGO68/edit?usp=sharing)

# Vidéo

+ Vidéo qui présente mon environnement de travail :
	- https://youtu.be/pFgIMphdu7w + qualité - musique de rap
    - https://youtu.be/o3T8xAs995Y - qualité + pas de rap


# Liens utile


- Installation de VMware Workstation 16 Player : 
    + https://www.vmware.com/fr/products/workstation-player/workstation-player-evaluation.html
***


- Installation de ma distribution : 
    + http://old-releases.ubuntu.com/releases/20.04.1/
***


- Installation du thème dracula :
	+ https://draculatheme.com/
	+ https://draculatheme.com/gtk
	+ https://draculatheme.com/gnome-terminal
	+ https://draculatheme.com/gedit
***

- Installation de Ubuntu 20.04 LTS sans environnement de bureau : 
    + https://www.microsoft.com/en-us/p/ubuntu-2004-lts/9n6svws3rx71?activetab=pivot:overviewtab

